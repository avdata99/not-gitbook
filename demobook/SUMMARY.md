# Summary

* [Chapter I](part1/README.md)
  * [Writing is nice](part1/writing.md)
  * [GitBook is nice](part1/gitbook.md)
* [Chapter II](part2/README.md)
  * [We love feedback](part2/feedback_please.md)
  * [Better tools for authors](part2/better_tools.md)

# More Stuff

* [Extra stuff](stuff.md)
* [Even more](more.md)