#!/usr/bin/env python

"""Not-Gitbook

Usage:
  notgitbook.py build [<book>] [<output>]
  notgitbook.py serve <book> <output>
  notgitbook.py init <book>
  notgitbook.py pdf <book>
  notgitbook.py mobi <book>
  notgitbook.py epub <book>
"""

import glob
import hashlib
import importlib.util
import logging
import os
import pprint
import shutil

import docopt
import jinja2
import mistune
from bs4 import BeautifulSoup

logging.basicConfig(level=logging.ERROR)

template_lookup = None


def setup_jinja(book_path):
    """Configure Jinja."""
    global template_lookup
    # TODO: cache
    template_lookup = jinja2.Environment()
    template_lookup.trim_blocks = True
    template_lookup.lstrip_blocks = True
    # FIXME also theme/templates relative to script location
    # TODO: support theme configuration / override
    locations = [
        os.path.join(book_path, "templates"), "themes/base/templates"
    ]
    logging.info(f"Template locations: {locations}")
    template_lookup.loader = jinja2.FileSystemLoader(
        locations, encoding="utf-8"
    )


def render_template(template_name, **context):
    """Render a Jinja template from path using context."""
    template = template_lookup.get_template(template_name)
    context['plugin_css_assets'] = list(plugin_css_assets.keys())
    data = template.render(**context)
    return data


markdown = None

plugin_css_assets = {}


def setup_md_renderer_plugins():
    global markdown, plugin_css_assets
    Renderer = mistune.Renderer
    # FIXME: relative to the script or book or something
    plugins = glob.glob("plugins/md_renderers/*/plugin.py")
    for plugin in plugins:
        p_name = os.path.basename(os.path.dirname(plugin))
        spec = importlib.util.spec_from_file_location(
            f"md_renderer.{p_name}", plugin
        )
        p_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(p_module)
        Renderer = p_module.chain_renderer(Renderer)
        css = p_module.css()
        hash = hashlib.md5(css.encode("utf-8")).hexdigest()
        plugin_css_assets[hash] = css
    markdown = mistune.Markdown(renderer=Renderer(escape=False))


def compile_markdown(source):
    """Compile markdown to HTML, return a string."""
    logging.info("Compiling %s", source)
    source = preproc_md_file(source)
    try:
        with open(source) as inf:
            html = markdown(inf.read())
    except FileNotFoundError:
        logging.error(f"Source not found: {source}")
        html = f"MISSING FILE {source}"
    return html


preproc_filters = []


def setup_md_preproc_filters():
    """Load all the md preproc filter plugins."""
    # FIXME: relative to the script or book or something
    plugins = glob.glob("plugins/md_preproc/*/plugin.py")
    for plugin in plugins:
        p_name = os.path.basename(os.path.dirname(plugin))
        spec = importlib.util.spec_from_file_location(
            f"md_preproc.{p_name}", plugin
        )
        p_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(p_module)
        p_module.filter.name = p_name
        preproc_filters.append(p_module.filter)


def preproc_md_file(source):
    """Apply filters to the given markdown file."""
    for filter in preproc_filters:
        logging.info(f"Processing {source} with {filter.name}")
        source = filter(source)
    return source


def build_markdown(source, dest, context):
    """Compile markdown to HTML, save in dest."""
    # Make sure output folder exists
    dest_dir = os.path.dirname(dest)
    if not os.path.isdir(dest_dir):
        logging.info(f"Creating {dest_dir}")
        os.makedirs(dest_dir)
    with open(dest, "w") as outf:
        content = compile_markdown(source)
        html = render_template("page.j2", content=content, **context)
        outf.write(html)
        logging.info("Saved to %s", dest)


def parse_summary_md(md_file):
    """Parse these lists gitbook uses in Markdown.

    Example:

    # Summary

    * [Part I](part1/README.md)
        * [Writing is nice](part1/writing.md)
        * [GitBook is nice](part1/gitbook.md)
    * [Part II](part2/README.md)
        * [We love feedback](part2/feedback_please.md)
        * [Better tools for authors](part2/better_tools.md)
    """

    # So, basically gitbook expects the following structure:
    # Parts, that start in titles
    #    Chapters, that are a list of links inside a part
    #        Subchapters, a list of links inside a chapter
    #
    # Parts are a title and a list of chapters
    # Chapters are a title, a link to a MD file, and a list of subchapters
    # Subchapters are a title and a link to a MD file

    # Idea: let's just make it HTML and parse the sucker, and not
    # bother with regexes.
    html = compile_markdown(md_file)
    doc = BeautifulSoup(html, features="html.parser")

    first_part = doc.find("ul")
    if first_part is None:
        logging.error("Summary has no content")
        return {}

    parts_list = [first_part] + first_part.find_next_siblings("ul")
    parts = []
    # The title for each part is a title before it
    for part in parts_list:
        title = part.find_previous_sibling("h1").text
        first_chapter = part.find("li")
        if first_chapter is None:
            continue

        chapters_list = [first_chapter] + first_chapter.find_next_siblings(
            "li"
        )
        chapters = []
        for chapter in chapters_list:
            item = chapter.find("a")
            chapter_title = item.text
            chapter_link = os.path.normpath(item.attrs["href"])
            # Yes, this will "support" deeper nesting by breaking it
            # I do not care at all.
            subchapters = chapter.find("ul")
            if subchapters:
                subchapters = [
                    [x.text, os.path.normpath(x.attrs["href"])]
                    for x in subchapters.find_all("a")
                ]
            else:
                subchapters = []
            chapters.append([chapter_title, chapter_link, subchapters])
        parts.append([title, chapters])

    # pretty-log it
    for part, chapters in parts:
        logging.info(f"part {part}")
        for title, md_file, subchapters in chapters:
            logging.info(f"  chapter: {title} - {md_file}")
            for title, md_file in subchapters:
                logging.info(f"    subchapter: {title} - {md_file}")
    return parts


def build(book_path, dest_path):
    """Build a book."""
    logging.info("Building book in %s", book_path)

    # Make sure input exists
    summary = os.path.join(book_path, "SUMMARY.md")
    if not os.path.isfile(summary):
        logging.error(f"Missing summary: {summary}")
        raise FileNotFoundError(summary)

    # Make sure output folder exists
    if not os.path.isdir(dest_path):
        logging.info(f"Creating {dest_path}")
        os.makedirs(dest_path)

    # Setup internal components
    setup_jinja(book_path)
    setup_md_preproc_filters()
    setup_md_renderer_plugins()

    # Parse summary document
    parts = parse_summary_md(summary)

    # Create linear list of chapters/subchapters
    flat_list = []
    for _, chapters in parts:
        # Part itself doesn't have content
        for title, md_file, subchapters in chapters:
            # FIXME: do proper extension replacement
            md_dest_path = os.path.join(
                dest_path, md_file.replace(".md", ".html")
            )
            offset = md_file.count("/")
            flat_list.append(
                [
                    os.path.join(book_path, md_file),
                    md_dest_path,
                    {"summary": parts, "offset": offset, "title": title},
                ]
            )
            for title, md_file in subchapters:
                offset = md_file.count("/")
                # FIXME: do proper extension replacement
                md_dest_path = os.path.join(
                    dest_path, md_file.replace(".md", ".html")
                )
                flat_list.append(
                    [
                        os.path.join(book_path, md_file),
                        md_dest_path,
                        {"summary": parts, "offset": offset, "title": title},
                    ]
                )

    # Add previous chapter data to context
    for i, chapter in enumerate(flat_list[1:], 1):
        cur_context = chapter[2]
        prev_chapter = flat_list[i - 1]
        cur_context["prev_title"] = prev_chapter[2]["title"]
        cur_context["prev"] = os.path.relpath(
            prev_chapter[1], os.path.dirname(chapter[1])
        )

    # Add next chapter data to context
    for i, chapter in enumerate(flat_list[:-1]):
        cur_context = chapter[2]
        next_chapter = flat_list[i + 1]
        cur_context["next_title"] = next_chapter[2]["title"]
        cur_context["next"] = os.path.relpath(
            next_chapter[1], os.path.dirname(chapter[1])
        )

    # Render all chapters and subchapters
    for src, dest, context in flat_list:
        build_markdown(src, dest, context=context)

    # Copy all relevant assets
    # First, the theme
    # TODO support themes properly
    copy_tree("themes/base/assets/", os.path.join(dest_path, "assets"))
    # Now CSS from plugins
    for k, v in plugin_css_assets.items():
        dst = os.path.join(dest_path, "styles", k + ".css")
        with open(dst, 'w') as outf:
            outf.write(v)

    # Now copy the source tree (includes images and so on)
    copy_tree(book_path, dest_path)


def copy_tree(src, dst):
    """Copy directory tree src to dst, overwrite anything."""
    if src[-1] != "/":
        src = src + "/"
    l = len(src)
    d_list = [
        d[l:] for d in glob.glob(os.path.join(src, "**/"), recursive=True)
    ]
    for d in d_list:
        if d:
            os.makedirs(os.path.join(dst, d), exist_ok=True)
    f_list = [
        f
        for f in glob.glob(os.path.join(src, "**"), recursive=True)
        if not os.path.isdir(f)
    ]
    for f in f_list:
        f_dst = os.path.join(dst, f[l:])
        logging.info(f"Copying {f} -> {f_dst}")
        shutil.copy2(f, f_dst)


if __name__ == "__main__":
    arguments = docopt.docopt(__doc__, version="Not-Gitbook v0.0.1")
    if arguments["build"]:
        book_path = arguments.get("<book>") or "."
        dest_path = arguments.get("<output>") or "_book"
        build(book_path, dest_path)
