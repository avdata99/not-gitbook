"""Plugin that preprocesses Markdown files via pyliterate."""

# This used to just be a Makefile rule like this:
# %.run.md: %.md $(wildcard code/%.py)
#         env PYTHONPATH=. run_markdown --timeout_seconds=15 --root_dir code/ $< > $@ || (rm -f $@ && false)

# But times change.

import logging
import os
import shlex
import subprocess


def filter(path):
    dst_path = path + ".pyliterate"
    cwd = os.path.dirname(path)
    f_name = os.path.basename(path)
    # Some heuristics because this is just for my book, really
    root_dir = os.path.join("code", f_name.split(".")[0])
    if not os.path.isdir(os.path.join(cwd, root_dir)):
        root_dir = "code"
        if not os.path.isdir(os.path.join(cwd, root_dir)):
            root_dir = "."
    my_env = os.environ.copy()
    my_env["PYTHONPATH"] = "."
    try:
        processed = subprocess.check_output(
            shlex.split(
                f"run_markdown --timeout_seconds=15 --root_dir {root_dir} {f_name}"
            ),
            cwd=cwd,
            env=my_env,
        )
        with open(dst_path, "wb") as outf:
            outf.write(processed)
        return dst_path

    except Exception as e:
        logging.error(f"Error processing {path}")
        logging.exception(e)
        raise
