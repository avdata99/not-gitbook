# Coding in Anger

So, I am pissed at gitbook. I am using it to write a book, and every tie I start
working on the book after dropping it for a week or six, something is broken, or
has changed just enough that I need to start figuring it out.

And figuring things out in gitbook is Not Fun.

So, fuck it, I am rewriting the thing. I have 24 hours, air conditioning, a
semi-understanding wife and coffee. Let's hit it.

The goal: something that makes books out of markdown, somewhat gitbook-compatible
and look-alike, in python, but that works at least well enough for my usecase.

## 19:40

* Created the project at https://gitlab.com/ralsina/not-gitbook
* Added basic requirements

## 19:52

* Basic usage for docopt

## 20:18

So, basically gitbook expects the following structure in SUMMARY.md:

* Parts, that start in titles
* Chapters, that are a list of links inside a part
* Subchapters, a list of links inside a chapter

* Parts are a title and a list of chapters
* Chapters are a title, a link to a MD file, and a list of subchapters
* Subchapters are a title and a link to a MD file

Idea: let's just make it HTML and parse the sucker.

# 20:28

* Added function to compile markdown via mistune
* Required BeautifulSoup4 just because I remember it as easier than lxml

# 20:44

* Apparent success parsing first level (parts)

# 21:00

* Parsed all levels of SUMMARY.md
* Made myself a sandwich

# 21:11

* Added a function to build markdown to a file
* Sanitized SUMMARY.md in example book
* Some logging

# 21:13

* Making dinner

# 22:00

* Had chicken milanesas and steamed potatos, and they were awesome

# 22:15

* Build demo book with no content "successfully"

# 22:19

* Starting work on the sidebar/TOC/Master doc thing.

# 22:45

* Jinja setup in place

# 22:55

* Successful build of index.html with correct links

# EOD

Enough for today!

# 9:52

Testing with my actual book, looks like there is a bug somewhere, since it's missing all the subchapters.

# 10:04

Fixed the problem with subchapters, time to start making it look nice, then processing plugins and then it's done.

# 10:09

Assets. I need to copy assets. That's CSS and whatever.

# 11:02

Had a hearty breakfast, added basic theme assets from gitbook

# 11:41

Implemented asset copying (it's annoying that there is no way to make shutil.copytree overwrite things)

# 12:09

Implemented basic page template and styling. Broken because of relative paths.

Also, gitbook seems to be doing some JS things which are really unnecessary.

# 13:37

Watched part of Bird Box, fixed positioning of summary nav sidebar.

# 13:41

Simplistic solution for relative CSS paths offset.

# 14:12

"Next" button works.

# 15:56

Pyliterate integration works! Next: highlighting using chroma, just because
I have too much stuff in my book that relies on it to look right.

# 16:35

Syntax highlighting via chroma works, all the kludgy extensions I added via pyliterate work... next: css injected by plugins

# 16:51

Generic CSS injection for plugins done. Good enough for now.